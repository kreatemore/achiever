var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

app.use('/app', express.static(__dirname + '/app/'));
app.use('/local', express.static(__dirname));
app.use('/images', express.static(__dirname + '/images/'));
app.use('/templates', express.static(__dirname + '/templates/'));
app.use('/node_modules', express.static(__dirname + '/node_modules/'));

app.get("/*", function (req, res) {
    res.sendFile(path.resolve(__dirname, 'index.html'));
});

module.exports = app;
