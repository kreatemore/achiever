"use strict";
var Goal = (function () {
    function Goal() {
        this.status = Goal.getStates().Pending;
    }
    Goal.getStates = function () {
        return {
            "Completed": "Completed",
            "Pending": "Pending",
            "Deleted": "Deleted"
        };
    };
    return Goal;
}());
exports.Goal = Goal;
//# sourceMappingURL=goal.model.js.map