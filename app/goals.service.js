"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var login_service_1 = require("./login.service");
var GoalService = (function () {
    function GoalService(loginService) {
        var _this = this;
        this.loginService = loginService;
        this.database = firebase.database().ref("goals/" + this.loginService.getUserId());
        this.goalArray = [];
        this.goals = new BehaviorSubject_1.BehaviorSubject([]);
        this.goalStream = this.goals.asObservable();
        this.subscription = this.loginService.userStream.subscribe(function (user) {
            _this.database = firebase.database().ref("goals/" + user.uid);
            _this.getGoals();
        });
    }
    GoalService.prototype.addGoal = function (goal) {
        this.database.push(goal);
    };
    GoalService.prototype.getGoals = function () {
        var _this = this;
        this.database.on("value", function (goals) { return _this.goals.next(_this.populateGoalsFromDatabase(goals)); }, function (error) { return console.error(error); });
    };
    GoalService.prototype.populateGoalsFromDatabase = function (goals) {
        this.goalArray = [];
        for (var id in goals.val()) {
            var goal = goals.val()[id];
            goal.id = id;
            if (id !== 'user')
                this.goalArray.push(goal);
        }
        return this.goalArray;
    };
    GoalService.prototype.removeGoal = function (goal) {
        this.database.child(goal.id).remove();
    };
    GoalService.prototype.updateGoal = function (goal) {
        var goalId = goal.id;
        this.database.child(goalId).update(goal);
    };
    GoalService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [login_service_1.LoginService])
    ], GoalService);
    return GoalService;
}());
exports.GoalService = GoalService;
//# sourceMappingURL=goals.service.js.map