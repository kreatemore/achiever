import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import {LoginService} from "./login.service";
import {User} from "./user.model";

declare var jQuery:any;

@Component({
    selector: 'login',
    templateUrl: '/templates/login.component.html',
})

export class LoginComponent implements OnInit {

    constructor(private loginService:LoginService) {
    }

    ngOnInit():any {
        //noinspection TypeScriptUnresolvedFunction
        jQuery("#loginModal").modal();
    }

    public static show():void {
        //noinspection TypeScriptUnresolvedFunction
        jQuery("#loginModal").modal('show');
    }

    public close():void {
        jQuery("#loginModal").modal('hide');
    }

    public loginWithProvider(providerName:string):void {
        this.loginService.auth(providerName);
    }
}