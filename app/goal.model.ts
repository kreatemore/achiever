export class Goal {
    constructor() {
        this.status = Goal.getStates().Pending;
    }

    public static getStates() {
        return {
            "Completed": "Completed",
            "Pending": "Pending",
            "Deleted": "Deleted"
        };
    }

    title:string;
    date:any;
    status:string;
    id:string;
}