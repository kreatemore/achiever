import {bootstrap}    from '@angular/platform-browser-dynamic';
import {AppComponent} from './app.component';
import {appRouterProviders} from "./app.routes";
//noinspection TypeScriptValidateTypes
bootstrap(AppComponent, [appRouterProviders]);