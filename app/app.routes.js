"use strict";
var router_1 = require('@angular/router');
var pageNotFound_component_1 = require("./pageNotFound.component");
var welcome_component_1 = require("./welcome.component");
var goals_component_1 = require("./goals.component");
var routes = [
    { path: 'home', component: welcome_component_1.WelcomeComponent },
    { path: 'goals', component: goals_component_1.GoalsComponent },
    { path: '**', component: pageNotFound_component_1.PageNotFoundComponent }
];
exports.appRouterProviders = [
    router_1.provideRouter(routes)
];
//# sourceMappingURL=app.routes.js.map