import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {User} from "./user.model";
import {CookieService} from "./cookie.service";

declare var firebase:any;

@Injectable()
export class LoginService {
    private authentication = firebase.auth;
    private user:User = new User;
    private currentUser = new BehaviorSubject<User>(new User);
    public  userStream = this.currentUser.asObservable();

    constructor(private cookieService:CookieService) {
        this.preloadUser();
    }

    private preloadUser() {
        let storedUser = this.cookieService.get("user");
        if (storedUser) {
            let user = new User;
            user = user.cloneFrom(JSON.parse(storedUser));
            this.setUser(user)
        }
    };

    public auth(providerName:string):void {
        this.authentication().signInWithPopup(this.getProviderByName(providerName))
            .then(result => this.setUser(result.user.providerData[0]))
            .catch(error => console.error(error));
    }

    public setUser(user:User):void {
        this.user = user;
        this.initUser(user);
        this.currentUser.next(user);
        this.cookieService.store("user", JSON.stringify(user));
    }

    private initUser(user) {
        var database = firebase.database().ref("goals/" + user.uid.toString());
        database.on('value', snapshot => {
            if (snapshot.val() === null) {
                database.set({user: user});
            }
        });
    };

    public getUserId():string {
        return this.user.uid;
    }

    public logout():void {
        this.cookieService.remove("user");
        this.currentUser.next(new User);
    }

    private getProviderByName(providerName) {
        let provider = null;
        if (providerName === 'facebook') {
            provider = new this.authentication.FacebookAuthProvider();
        } else if (providerName === 'google') {
            provider = new this.authentication.GoogleAuthProvider();
        } else {
            console.error("Unsupported login provider");
        }
        return provider;
    };

}
