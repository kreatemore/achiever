import { provideRouter, RouterConfig } from '@angular/router';
import {AppComponent} from "./app.component";
import {PageNotFoundComponent} from "./pageNotFound.component";
import {WelcomeComponent} from "./welcome.component";
import {GoalsComponent} from "./goals.component";

const routes:RouterConfig = [
    {path: 'home', component: WelcomeComponent},
    {path: 'goals', component: GoalsComponent},
    {path: '**', component: PageNotFoundComponent}
];

export const appRouterProviders = [
    provideRouter(routes)
];