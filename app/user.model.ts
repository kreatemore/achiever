export class User {
    displayName:string;
    email:string;
    photoURL:string;
    providerId:string;
    uid:string;

    public cloneFrom(object:any) {
        let user = new User;
        user.displayName = object.displayName;
        user.email = object.email;
        user.photoURL = object.photoURL;
        user.providerId = object.providerId;
        user.uid = object.uid;
        return user;
    }
}