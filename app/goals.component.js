"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var goal_model_1 = require("./goal.model");
var goals_service_1 = require("./goals.service");
var login_service_1 = require("./login.service");
var search_pipe_1 = require("./search.pipe");
var GoalsComponent = (function () {
    function GoalsComponent(goalService, loginService) {
        this.goalService = goalService;
        this.loginService = loginService;
        this.goals = [];
        this.goalInError = false;
        this.isAuthenticated = false;
        this.goal = new goal_model_1.Goal;
    }
    GoalsComponent.prototype.ngOnInit = function () {
        var _this = this;
        var self = this;
        //noinspection TypeScriptUnresolvedFunction
        jQuery(".progress").progress();
        jQuery(".editable.content input").keyup(function () {
            var editedGoal = new goal_model_1.Goal;
        });
        this.goalService.getGoals();
        this.goalSubscription = this.goalService.goalStream.subscribe(function (goals) {
            _this.goals = goals;
            setTimeout(function () { return self.updateGoalProgress(); }, 250);
        });
        this.userSubscription = this.loginService.userStream.subscribe(function (user) { return _this.isAuthenticated = user.hasOwnProperty("uid"); });
    };
    GoalsComponent.prototype.ngOnDestroy = function () {
        this.goalSubscription.unsubscribe();
        this.userSubscription.unsubscribe();
    };
    GoalsComponent.prototype.addGoal = function (goal) {
        this.goalInError = false;
        if (!goal.title) {
            this.goalInError = true;
            jQuery("input[name='goal-title']").transition('pulse');
            return;
        }
        if (typeof goal.date === 'undefined')
            this.setGoalDate(31);
        this.goalService.addGoal(goal);
        this.updateGoalProgress();
        this.goal = new goal_model_1.Goal;
    };
    GoalsComponent.prototype.setDueDate = function (daysToAdd) {
        var dueDate = new Date();
        var year = dueDate.getFullYear();
        var month = dueDate.getMonth() + 1;
        var day = dueDate.getDate();
        var numberOfDaysInMonth = new Date(dueDate.getFullYear(), dueDate.getMonth(), 0).getDate();
        if (day + daysToAdd > numberOfDaysInMonth) {
            month += 1;
        }
        else {
            day += daysToAdd;
        }
        var finalMonth = month.toString();
        var finalDay = day.toString();
        if (month < 10)
            finalMonth = "0" + month.toString();
        if (day < 10)
            finalDay = "0" + day.toString();
        return year + "-" + finalMonth + "-" + finalDay;
    };
    ;
    GoalsComponent.prototype.completeGoal = function (goal) {
        goal.status = goal_model_1.Goal.getStates().Completed;
        this.goalService.updateGoal(goal);
    };
    GoalsComponent.prototype.deleteGoal = function (goal) {
        this.goalService.removeGoal(goal);
    };
    GoalsComponent.prototype.resumeGoal = function (goal) {
        goal.status = goal_model_1.Goal.getStates().Pending;
        this.goalService.updateGoal(goal);
    };
    GoalsComponent.prototype.updateGoal = function (goal) {
        this.goalService.updateGoal(goal);
    };
    GoalsComponent.prototype.editGoal = function (goal) {
        goal.title = jQuery("#title-for-" + goal.id).val();
        goal.date = jQuery("#date-for-" + goal.id).val();
        this.updateGoal(goal);
    };
    GoalsComponent.prototype.updateGoalProgress = function () {
        //noinspection TypeScriptUnresolvedFunction
        jQuery(".progress").progress({ percent: this.getCompletedGoalsPercentage() });
    };
    GoalsComponent.prototype.getCompletedGoalsPercentage = function () {
        if (!this.goals || this.goals.length === 0)
            return "0%";
        var percentage = (this.getCompletedGoals() / this.goals.length) * 100;
        return percentage + "%";
    };
    GoalsComponent.prototype.getCompletedGoalsPercentageAsNumber = function () {
        return parseInt(this.getCompletedGoalsPercentage(), 10);
    };
    GoalsComponent.prototype.getCompletedGoals = function () {
        var completed = 0;
        for (var _i = 0, _a = this.goals; _i < _a.length; _i++) {
            var registeredGoal = _a[_i];
            if (registeredGoal.status === "Completed")
                completed++;
        }
        return completed;
    };
    ;
    GoalsComponent.prototype.getOutstandingGoals = function () {
        return this.goals.length - this.getCompletedGoals();
    };
    GoalsComponent.prototype.getDateFromGoal = function (goal) {
        return new Date(goal.date).toLocaleDateString();
    };
    GoalsComponent.prototype.setGoalDate = function (when) {
        this.goal.date = this.setDueDate(when);
    };
    GoalsComponent.prototype.scrollToView = function (selector) {
        var offset = jQuery(selector).offset();
        jQuery('html, body').animate({
            scrollTop: offset.top
        });
    };
    GoalsComponent = __decorate([
        core_1.Component({
            selector: 'goals',
            templateUrl: '/templates/goals.component.html',
            pipes: [search_pipe_1.SearchPipe],
            providers: [goals_service_1.GoalService]
        }), 
        __metadata('design:paramtypes', [goals_service_1.GoalService, login_service_1.LoginService])
    ], GoalsComponent);
    return GoalsComponent;
}());
exports.GoalsComponent = GoalsComponent;
//# sourceMappingURL=goals.component.js.map