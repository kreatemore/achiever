"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
var user_model_1 = require("./user.model");
var cookie_service_1 = require("./cookie.service");
var LoginService = (function () {
    function LoginService(cookieService) {
        this.cookieService = cookieService;
        this.authentication = firebase.auth;
        this.user = new user_model_1.User;
        this.currentUser = new BehaviorSubject_1.BehaviorSubject(new user_model_1.User);
        this.userStream = this.currentUser.asObservable();
        this.preloadUser();
    }
    LoginService.prototype.preloadUser = function () {
        var storedUser = this.cookieService.get("user");
        if (storedUser) {
            var user = new user_model_1.User;
            user = user.cloneFrom(JSON.parse(storedUser));
            this.setUser(user);
        }
    };
    ;
    LoginService.prototype.auth = function (providerName) {
        var _this = this;
        this.authentication().signInWithPopup(this.getProviderByName(providerName))
            .then(function (result) { return _this.setUser(result.user.providerData[0]); })
            .catch(function (error) { return console.error(error); });
    };
    LoginService.prototype.setUser = function (user) {
        this.user = user;
        this.initUser(user);
        this.currentUser.next(user);
        this.cookieService.store("user", JSON.stringify(user));
    };
    LoginService.prototype.initUser = function (user) {
        var database = firebase.database().ref("goals/" + user.uid.toString());
        database.on('value', function (snapshot) {
            if (snapshot.val() === null) {
                database.set({ user: user });
            }
        });
    };
    ;
    LoginService.prototype.getUserId = function () {
        return this.user.uid;
    };
    LoginService.prototype.logout = function () {
        this.cookieService.remove("user");
        this.currentUser.next(new user_model_1.User);
    };
    LoginService.prototype.getProviderByName = function (providerName) {
        var provider = null;
        if (providerName === 'facebook') {
            provider = new this.authentication.FacebookAuthProvider();
        }
        else if (providerName === 'google') {
            provider = new this.authentication.GoogleAuthProvider();
        }
        else {
            console.error("Unsupported login provider");
        }
        return provider;
    };
    ;
    LoginService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [cookie_service_1.CookieService])
    ], LoginService);
    return LoginService;
}());
exports.LoginService = LoginService;
//# sourceMappingURL=login.service.js.map