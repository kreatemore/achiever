import {Component} from "@angular/core";
@Component({
    selector: "missingPage",
    template: "This is a sample 404 page"
})

export class PageNotFoundComponent {
}