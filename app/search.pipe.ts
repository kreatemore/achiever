import {Pipe, PipeTransform} from "@angular/core";
import {Goal} from "./goal.model";

@Pipe({
    name: 'search'
})

export class SearchPipe implements PipeTransform {
    transform(goals:Goal[], value:string):any {
        return goals.filter(goal => {
            if (!value) return true;
            let searchExpression = new RegExp(".*" + value + ".*", "ig");
            return searchExpression.test(goal.title);
        });
    }

}