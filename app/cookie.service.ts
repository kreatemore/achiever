import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Subscription} from "rxjs/Subscription";

@Injectable()
export class CookieService {
    public store(name:string, value:any):void {
        document.cookie = name + "=" + value + ";";
    }

    public get(name:string):any {
        let cookies = document.cookie.split("; ");
        for (let cookie of cookies) {
            if (cookie.split("=")[0] == name)
                return cookie.split(name + "=")[1];
        }
    }

    public remove(name:string):void {
        document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:01 GMT;";
    }
}