"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var navigation_component_1 = require("./navigation.component");
var welcome_component_1 = require("./welcome.component");
var pageNotFound_component_1 = require("./pageNotFound.component");
var goals_component_1 = require("./goals.component");
var cookie_service_1 = require("./cookie.service");
var AppComponent = (function () {
    function AppComponent() {
    }
    AppComponent = __decorate([
        core_1.Component({
            selector: 'achiever-app',
            templateUrl: '/templates/app.component.html',
            directives: [navigation_component_1.NavigationComponent],
            providers: [cookie_service_1.CookieService],
            precompile: [welcome_component_1.WelcomeComponent, goals_component_1.GoalsComponent, pageNotFound_component_1.PageNotFoundComponent]
        }), 
        __metadata('design:paramtypes', [])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map