"use strict";
var User = (function () {
    function User() {
    }
    User.prototype.cloneFrom = function (object) {
        var user = new User;
        user.displayName = object.displayName;
        user.email = object.email;
        user.photoURL = object.photoURL;
        user.providerId = object.providerId;
        user.uid = object.uid;
        return user;
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.model.js.map