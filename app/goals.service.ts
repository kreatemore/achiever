import {Injectable} from "@angular/core";
import {BehaviorSubject} from "rxjs/BehaviorSubject";
import {Goal} from "./goal.model";
import {LoginService} from "./login.service";
import {Subscription} from "rxjs/Subscription";

declare var firebase:any;

@Injectable()
export class GoalService {
    private subscription:Subscription;
    private userId:string;
    private database = firebase.database().ref("goals/" + this.loginService.getUserId());
    private goalArray:Goal[] = [];
    public goals = new BehaviorSubject<Array<Goal>>([]);
    public goalStream = this.goals.asObservable();

    constructor(private loginService:LoginService) {
        this.subscription = this.loginService.userStream.subscribe(
            user => {
                this.database = firebase.database().ref("goals/" + user.uid);
                this.getGoals();
            }
        );
    }

    public addGoal(goal:Goal) {
        this.database.push(goal);
    }

    public getGoals():any {
        this.database.on("value",
            goals => this.goals.next(this.populateGoalsFromDatabase(goals)),
            error => console.error(error));
    }

    private populateGoalsFromDatabase(goals) {
        this.goalArray = [];
        for (var id in goals.val()) {
            let goal = goals.val()[id];
            goal.id = id;
            if (id !== 'user') this.goalArray.push(goal);
        }
        return this.goalArray;
    }

    public removeGoal(goal:Goal) {
        this.database.child(goal.id).remove();
    }

    public updateGoal(goal:Goal) {
        let goalId = goal.id;
        this.database.child(goalId).update(goal);
    }
}