import {Component, ViewChild, OnInit} from '@angular/core';
import {Goal} from "./goal.model";
import {GoalService} from "./goals.service";
import {Subscription} from "rxjs/Subscription";
import {User} from "./user.model";
import {LoginService} from "./login.service";
import {SearchPipe} from "./search.pipe";

declare var jQuery:any;

@Component({
    selector: 'goals',
    templateUrl: '/templates/goals.component.html',
    pipes: [SearchPipe],
    providers: [GoalService]
})

export class GoalsComponent implements OnInit {
    private goals:Goal[] = [];
    private goalSubscription:Subscription;
    private userSubscription:Subscription;
    private goalInError:boolean = false;
    private isAuthenticated:boolean = false;
    public goal:Goal = new Goal;

    constructor(private goalService:GoalService, private loginService:LoginService) {
    }

    ngOnInit():void {
        var self = this;
        //noinspection TypeScriptUnresolvedFunction
        jQuery(".progress").progress();
        jQuery(".editable.content input").keyup(() => {
            let editedGoal = new Goal;
        });
        this.goalService.getGoals();
        this.goalSubscription = this.goalService.goalStream.subscribe(
            goals => {
                this.goals = goals;
                setTimeout(() => self.updateGoalProgress(), 250);
            }
        );
        this.userSubscription = this.loginService.userStream.subscribe(
            user => this.isAuthenticated = user.hasOwnProperty("uid")
        );
    }

    ngOnDestroy():void {
        this.goalSubscription.unsubscribe();
        this.userSubscription.unsubscribe();
    }

    public addGoal(goal:Goal):void {
        this.goalInError = false;
        if (!goal.title) {
            this.goalInError = true;
            jQuery("input[name='goal-title']").transition('pulse');
            return;
        }
        if (typeof goal.date === 'undefined') this.setGoalDate(31);
        this.goalService.addGoal(goal);
        this.updateGoalProgress();
        this.goal = new Goal;
    }

    private setDueDate(daysToAdd:number) {
        let dueDate = new Date();
        let year = dueDate.getFullYear();
        let month = dueDate.getMonth() + 1;
        let day = dueDate.getDate();

        let numberOfDaysInMonth = new Date(dueDate.getFullYear(), dueDate.getMonth(), 0).getDate();
        if (day + daysToAdd > numberOfDaysInMonth) {
            month += 1;
        } else {
            day += daysToAdd;
        }

        let finalMonth = month.toString();
        let finalDay = day.toString();
        if (month < 10) finalMonth = "0" + month.toString();
        if (day < 10) finalDay = "0" + day.toString();

        return year + "-" + finalMonth + "-" + finalDay;
    };

    public completeGoal(goal:Goal):void {
        goal.status = Goal.getStates().Completed;
        this.goalService.updateGoal(goal);
    }

    public deleteGoal(goal:Goal):void {
        this.goalService.removeGoal(goal);
    }

    public resumeGoal(goal:Goal):void {
        goal.status = Goal.getStates().Pending;
        this.goalService.updateGoal(goal);
    }

    public updateGoal(goal:Goal):void {
        this.goalService.updateGoal(goal);
    }

    public editGoal(goal:Goal):void {
        goal.title = jQuery("#title-for-" + goal.id).val();
        goal.date = jQuery("#date-for-" + goal.id).val();
        this.updateGoal(goal);
    }

    private updateGoalProgress():void {
        //noinspection TypeScriptUnresolvedFunction
        jQuery(".progress").progress({percent: this.getCompletedGoalsPercentage()});
    }

    public getCompletedGoalsPercentage():string {
        if (!this.goals || this.goals.length === 0) return "0%";
        let percentage = (this.getCompletedGoals() / this.goals.length) * 100;
        return percentage + "%";
    }

    public getCompletedGoalsPercentageAsNumber():number {
        return parseInt(this.getCompletedGoalsPercentage(), 10);
    }

    public getCompletedGoals() {
        let completed = 0;
        for (let registeredGoal of this.goals) {
            if (registeredGoal.status === "Completed") completed++;
        }
        return completed;
    };

    public getOutstandingGoals() {
        return this.goals.length - this.getCompletedGoals();
    }

    public getDateFromGoal(goal) {
        return new Date(goal.date).toLocaleDateString();
    }

    public setGoalDate(when:number) {
        this.goal.date = this.setDueDate(when);
    }

    public scrollToView(selector:string) {
        var offset = jQuery(selector).offset();
        jQuery('html, body').animate({
            scrollTop: offset.top
        });
    }
}