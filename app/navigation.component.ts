import { Component, OnInit } from '@angular/core';
import { ROUTER_DIRECTIVES } from '@angular/router';
import {LoginComponent} from "./login.component";
import {LoginService} from "./login.service";
import {Subscription} from "rxjs/Subscription";
import {User} from "./user.model";

declare var jQuery:any;

@Component({
    selector: 'navigation',
    templateUrl: '/templates/navigation.component.html',
    directives: [ROUTER_DIRECTIVES, LoginComponent],
    providers: [LoginService]
})

export class NavigationComponent implements OnInit {
    private subscription:Subscription;
    public user:User;

    constructor(private loginService:LoginService) {
    }

    ngOnInit():any {
        this.subscription = this.loginService.userStream.subscribe(
            user => {
                this.user = user;
                jQuery("#loginModal").modal('hide');
            }
        );
    }

    ngOnDestroy():any {
        this.subscription.unsubscribe();
    }

    public showLoginModal() {
        LoginComponent.show();
    }

    public logout():void {
        this.loginService.logout();
    }
}