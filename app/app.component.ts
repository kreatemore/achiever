import {Component} from '@angular/core';
import {NavigationComponent} from "./navigation.component";
import {WelcomeComponent} from "./welcome.component";
import {PageNotFoundComponent} from "./pageNotFound.component";
import {GoalsComponent} from "./goals.component";
import {CookieService} from "./cookie.service";
import {LoginService} from "./login.service";
import {User} from "./user.model";

@Component({
    selector: 'achiever-app',
    templateUrl: '/templates/app.component.html',
    directives: [NavigationComponent],
    providers: [CookieService],
    precompile: [WelcomeComponent, GoalsComponent, PageNotFoundComponent]
})

export class AppComponent {
}